import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class SqliteService {
  dbName: SQLiteObject;
  getvisitors: any;
  // url ="http://newsapi.org/v2/everything?q=bitcoin&from=2020-05-17&sortBy=publishedAt&apiKey=40b66358733943d7beb5609525822f6c";

   url ="http://newsapi.org/v2/everything?q=bitcoin&sortBy=publishedAt&apiKey=40b66358733943d7beb5609525822f6c";

  constructor(private sqlite: SQLite, private http: HttpClient,) { }

  createDatabase() {
    this.sqlite.create({
      name: 'visitors.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      console.log("created", db);
      this.dbName = db;
      this.createlisttable();
    })
  }
  
  createlisttable() {
    return new Promise((resolve, reject) => {
      // alert("table created");
      this.dbName.open().then((res) => {
        let query = "CREATE TABLE IF NOT EXISTS visitor_list(name VARCHAR(256), email VARCHAR(256), visit INTEGER(11), visitperson VARCHAR(256), entrydate VARCHAR(256), entrytime VARCHAR(256), exittime VARCHAR(256) );";
        this.dbName.executeSql(query, []).then((res) => {
          resolve(res);
          console.log("table", res);
          // alert("table created");
        }).catch((err) => {
          // alert(JSON.stringify(err))
          console.log(err);
          reject(err);
        })
      });
    })
  }

  insertintolist(data) {
    console.log("add course", data)
    return new Promise((resolve, reject) => {
      this.dbName.open().then((res) => {
        let query = "INSERT INTO visitor_list(name, email, visit, visitperson, entrydate, entrytime, exittime) VALUES (?,?,?,?,?,?,?);";
        this.dbName.executeSql(query, [data.name, data.email, data.visit, data.visitperson, data.entrydate, data.entrytime, data.exittime]).then((res) => {
          // alert(JSON.stringify(res));
          resolve(res);
        }).catch((err) => {
          // alert(err);
          console.log(err);
          console.log("insertR", err);
          reject(err);
        })
      });
    })
  }

  getlist() {
    this.getvisitors = [];
    return new Promise((resolve, reject) => {
      this.dbName.open().then((res) => {
        let query = "SELECT * FROM visitor_list";
        this.dbName.executeSql(query, []).then((res) => {
          // resolve(res);
          alert(JSON.stringify(res.rows));
          for (let i = 0; i < res.rows.length; i++) {
            var list = JSON.stringify(res.rows.item(i));
            this.getvisitors.push(JSON.parse(list));
          }
          resolve(this.getvisitors);
          alert("fetch");
        }).catch((err) => {
          // alert(JSON.stringify(err));
          console.log(err);
          resolve(err);
          reject(err);
        })
      });
    })
  }


  getnews(cdate) : Promise<any> {
    return this.http.get(this.url + "&from="+cdate)
    .toPromise()
    .then(response => {
    return response = response; 
    }, err=>{
      return err;
    })
  }
}
