import { Component, OnInit } from '@angular/core';
import { SqliteService } from './../../provider/sqlite.service'
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {
  news: any = [];
  inAppBrowserRef: any;
  loading:any;
  oldnews:any = [];
  constructor(public Sqlservice: SqliteService, private iab: InAppBrowser,
    public loadingController: LoadingController) { }

  ngOnInit() {
    this.newslist();
  }

  newslist() {
    // this.presentLoading();
    let cdate = this.formatdate(new Date);
    this.Sqlservice.getnews(cdate).then(res => {
      console.log(res);
      if (res.status == "ok") {
        this.news = res.articles;
        // this.paginationnews();
        console.log(this.news);
      }
      // this.loaddismis();
    }, err => {
      console.log(err);
    })
  }

  formatdate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [year, month, day].join('-');
  }

  open_browser(data) {
    console.log(data);
    // this.inAppBrowserOptions
    this.inAppBrowserRef = this.iab.create(data.url, '_blank', );
  
  }


  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Please wait...',
      // duration: 2000
    });
    this.loading.present();

    // const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }
  loaddismis(){
    this.loading.dismiss();
  }

  paginationnews(){
    // this.oldnews =[]
    this.oldnews.push(this.news.splice(0,5))
    console.log(this.oldnews);
  }
}
