import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { SqliteService } from './../../provider/sqlite.service'
import { ToastController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-visitors',
  templateUrl: './visitors.page.html',
  styleUrls: ['./visitors.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class VisitorsPage implements OnInit {
  loading: any; 
  visitorsForm: FormGroup;
  visitors:any = [];
  // visitors = [
  //   { id:1,
  //   name: 'NIkhil',
  //   date: 25/12/2010 },
  //   { id:2,
  //     name: 'NIkhil',
  //     date: 25/12/2010 },
  //   { id:3,
  //     name: 'NIkhil',
  //     date: 25/12/2010 },
  //   { id:4,
  //     name: 'NIkhil',
  //     date: 25/12/2010 },
  //   { id:5,
  //     name: 'NIkhil',
  //     date: 25/12/2010 }    
  // ]
  user = {
    email: "",
    entrydate: "",
    entrytime: "",
    exittime: "",
    name: "",
    visit: "",
    visitperson: "",
  }
  list: boolean = false;
  date = new Date();

  visittype = [
    {
      id: 1,
      title: 'Meeting'
    },
    {
      id: 2,
      title: 'Delievery'
    },
    {
      id: 3,
      title: 'personal'
    }
  ]

  constructor( private fb: FormBuilder, public Sqlservice: SqliteService,
     public toastController: ToastController, public loadingController: LoadingController) { 
  }

  ngOnInit() {
    this.visitorslist();
    this.user.entrydate = this.formatdate(this.date);
    // alert(this.user.entrydate);
    // this.createvisitorsform();
  }

  formatdate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [day, month, year].join('-');
}
  visitorslist(){
    // this.presentLoading();
    this.Sqlservice.getlist().then(res=>{
      console.log(res);
      this.visitors = res;
      // this.loaddismis();
    }, err => {
      // this.loaddismis();
      console.log(err)
    })
  }


  add(data){
    console.log(data);
    if(data){
      this.createvisitorsform(data);
    } else{
      this.createvisitorsform(this.user);
    }
    
    this.list = true;
  }
  
  createvisitorsform(data){
    // alert(JSON.stringify(data));
    this.visitorsForm = this.fb.group({
      name: [data.name, Validators.compose([Validators.required])],
      email: [data.email, Validators.compose([Validators.required])],
      visit: [data.visit, Validators.compose([Validators.required])],
      visitperson: [data.visitperson, Validators.compose([Validators.required])],
      entrydate: [data.entrydate, Validators.compose([Validators.required])],
      entrytime: [data.entrytime, Validators.compose([Validators.required])],
      exittime: [data.exittime, Validators.compose([Validators.required])],
    });
  }
  submit(){
    // this.presentLoading();
    console.log(this.visitorsForm.value);
    if (this.visitorsForm.valid){
      this.Sqlservice.insertintolist(this.visitorsForm.value);
      this.visitorslist();
      this.list = false;
      // this.loaddismis();
      this.presentToast();
    } else{
      this.validateAllFormFields(this.visitorsForm);
    }
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'You have successfully added the visitor.',
      duration: 2000
    });
    toast.present();
  }
  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Please wait...',
      // duration: 2000
    });
    this.loading.present();

    // const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }
  loaddismis(){
    this.loading.dismiss();
  }
}
